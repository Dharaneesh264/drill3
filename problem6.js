const problem6 = (inventory) => {
    const result = [];
    if (inventory === undefined || inventory.length === 0) 
        return result;
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_make == 'BMW' || inventory[i].car_make === 'Audi') {
            result.push(inventory[i]);
        }
    }
    return result;
}

module.exports = problem6;