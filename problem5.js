const problem5 = (inventory) => {

    let answer = 0;
    if (inventory === undefined || inventory.length === 0) {
        return answer;
    }
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_year < 2000) {
            answer++;
        }
    }
    return answer;
}

module.exports = problem5;