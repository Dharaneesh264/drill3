const problem2 = (inventory) => {
    let result = [];
    if (inventory === undefined || inventory.length === 0) 
        return result;
    let lastIndex = inventory.length - 1;
    return "Last car is a " + inventory[lastIndex].car_make + " " + inventory[lastIndex].car_model;
    // result.push(answer);
    // return result;
}


module.exports = problem2;