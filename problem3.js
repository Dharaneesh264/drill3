const problem3 = (inventory) => {
    var carModels = [];
    if (inventory === undefined || inventory.length === 0) 
    return carModels;
    for (let i = 0; i < inventory.length; i++) {
        carModels.push(inventory[i].car_model);
    }
    carModels.sort(
        (a, b) => {
            return a.toLowerCase().localeCompare(b.toLowerCase());
        }
    );
    return carModels;
}

module.exports = problem3;