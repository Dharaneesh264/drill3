
const problem4 = (inventory) => {
    var answer = [];
    if (inventory === undefined || inventory.length === 0) 
        return answer;
    for (let i = 0; i < inventory.length; i++) {
       answer.push(inventory[i].car_year);
    }
    return answer;
}

module.exports = problem4;